# ShareChannels for PushBullet #

A simple Android Application which works with PushBullet, helps you to get details of a Channel and allow you to subscribe/unsubscribe. Pushbullet App does not have such feature, so I wrote this app to learn Rx, Retrofit and eventually Databinding and Design Support library with a working app.


### How do I get set up? ###

* [Create your client on PushBullet](https://www.pushbullet.com/create-client)
* While creating your client, pass a redirect url.
* Create `gradle.properties` file in your project. 
* Put following keys in properties file
	* `PUSHBULLET_CLIENT_ID = {clientId from created client}`
	* `PUSHBULLET_REDIRECTION_URL ={redirection url}`
* Compile and you are good to go.

### What if I don't want to use CrashLytics  ###
Remove following lines from your gradle files.
 
 * In project's `build.gradle` file
	 * classpath 'io.fabric.tools:gradle:1.19.1'
	 *  maven { url 'https://maven.fabric.io/public' }
	 *  apply plugin: 'io.fabric'

 * In module's `build.gradle` file
	 * apply plugin: 'io.fabric'
	 *  compile('com.crashlytics.sdk.android:crashlytics:2.3.2@aar') {
        transitive = true;
    	}
 * In MainActivity.java
	 * Crashlytics crashlytics = new Crashlytics();
		crashlytics.core.setString("CodeRevision", BuildConfig.GIT_SHA);
		Fabric.with(this, crashlytics);

### What if I don't use git or does not have git configured in command line? ###
Well it's easy to have both but if you still want to go that way,open `build.gradle` of module and replace this line

`def gitSha = 'git rev-parse --short HEAD'.execute([], project.rootDir).text.trim()`

with this line

`def gitSha =''`

Above line requires less changes. But if you want to remove gitSha completely you have to remove following lines.

 * In build.gradle
	 * `buildConfigField "String", "GIT_SHA", "\"$gitSha\""`

 * In MainActivity
	 * `crashlytics.core.setString("CodeRevision", BuildConfig.GIT_SHA);`


### Third party libraries and plugins ###
 
 * [Android Databinding plugin](https://developer.android.com/tools/data-binding/guide.html)
 * [ButterKnife](http://jakewharton.github.io/butterknife/)
 * [Retrofit](http://square.github.io/retrofit/)
 * [Glide](https://github.com/bumptech/glide/)
 * [DevUtils](https://github.com/CreativeElites/UtilsLibrary) - Personal Utils library
 * [RxAndroid](https://github.com/ReactiveX/RxAndroid)
 * [SharedPreferenceInspector](https://github.com/PrashamTrivedi/SharedPreferenceInspector)
 * [Faker](https://github.com/thiagokimo/Faker) - To provide fake names, images for play store listing
 * CardView and RecyclerView support libraries
 * Design Support Library
 * CrashLytics (fabric framework)

### What's next? ###

 * Writing Tests
 * Write Comments.

## License
   	   Copyright 2015 Prasham Trivedi
   	   Licensed under the Apache License, Version 2.0 (the "License");
   	   you may not use this file except in compliance with the License.
   	   You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

   	   Unless required by applicable law or agreed to in writing, software
   	   distributed under the License is distributed on an "AS IS" BASIS,
   	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   	   See the License for the specific language governing permissions and
   	   limitations under the License.

## Contact Me ##
 * [My Website](http://prashamtrivedi.github.io/)
 * [Google Plus](https://plus.google.com/+PrashamTrivedi)