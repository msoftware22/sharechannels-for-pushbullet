package com.celites.sharechannels.utils;

import android.os.Parcel;

/**
 * Created by Prasham on 5/14/2015.
 */
public class PushRequest
		implements android.os.Parcelable {

	String email;
	String type;
	String title;
	String body;
	String url;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.email);
		dest.writeString(this.type);
		dest.writeString(this.title);
		dest.writeString(this.body);
		dest.writeString(this.url);
	}

	public PushRequest() {
	}

	private PushRequest(Parcel in) {
		this.email = in.readString();
		this.type = in.readString();
		this.title = in.readString();
		this.body = in.readString();
		this.url = in.readString();
	}

	public static final Creator<PushRequest> CREATOR = new Creator<PushRequest>() {
		public PushRequest createFromParcel(Parcel source) {
			return new PushRequest(source);
		}

		public PushRequest[] newArray(int size) {
			return new PushRequest[size];
		}
	};

	public static PushRequest creteFromDefault() {
		PushRequest request = new PushRequest();
		request.setBody("Invitation To subscribe a channel");
		request.setType("link");
		return request;
	}
}
