package com.celites.sharechannels.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Inspired from AttractionsRecycler view from XYZTouristAttractionsDemo Created by Prasham on 6/19/2015.
 */
public class LoadingRecyclerView
		extends RecyclerView {

	private View emptyView;

	private boolean isLoading;

	private Context context;
	private TextView emptyMessage;
	private View loadingView;

	public LoadingRecyclerView(Context context) {
		this(context, null);

	}

	public LoadingRecyclerView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public LoadingRecyclerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}

	private void init() {
		isLoading = true;
		setEmptyView(new ProgressBar(context));
		emptyMessage = new TextView(context);

	}

	public void setEmptyView(View emptyView) {
		this.emptyView = emptyView;
	}

	public void setLoadingView(View loadingView) {
		this.loadingView = loadingView;
	}

	@Override
	public void setAdapter(RecyclerView.Adapter adapter) {
		if (getAdapter() != null) {
			getAdapter().unregisterAdapterDataObserver(mDataObserver);
		}
		if (adapter != null) {
			adapter.registerAdapterDataObserver(mDataObserver);
		}
		super.setAdapter(adapter);
		if (emptyView == null) {
			emptyView = emptyMessage;
		}
		updateEmptyView();
	}

	public void updateEmptyView() {
		if (emptyView != null) {
			if (getAdapter() != null) {
				boolean showEmptyView = getAdapter().getItemCount() == 0;
				loadingView.setVisibility(GONE);
				emptyView.setVisibility(showEmptyView ? VISIBLE : GONE);
				setVisibility(showEmptyView ? GONE : VISIBLE);
			} else {
				loadingView.setVisibility(GONE);
				emptyView.setVisibility(VISIBLE);
				setVisibility(GONE);
			}
		}
	}

	private AdapterDataObserver mDataObserver = new AdapterDataObserver() {
		@Override
		public void onChanged() {
			super.onChanged();
			updateEmptyView();
		}
	};

	public void updateLoadingView() {
		if (loadingView != null) {
			if (isLoading) {
				emptyView.setVisibility(GONE);
				setVisibility(GONE);
				loadingView.setVisibility(VISIBLE);
			} else {
				loadingView.setVisibility(GONE);
				updateEmptyView();
			}
		}
	}


}
