package com.celites.sharechannels.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Prasham on 4/25/2015.
 */
public class Contacts {

	private boolean active;

	private String iden;

	private float created;

	private float modified;

	private String name;

	private String email;

	@SerializedName("email_normalized")
	private String emailNormalized;

	private String status;

	/**
	 * @return The active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 * 		The active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return The iden
	 */
	public String getIden() {
		return iden;
	}

	/**
	 * @param iden
	 * 		The iden
	 */
	public void setIden(String iden) {
		this.iden = iden;
	}

	/**
	 * @return The created
	 */
	public float getCreated() {
		return created;
	}

	/**
	 * @param created
	 * 		The created
	 */
	public void setCreated(float created) {
		this.created = created;
	}

	/**
	 * @return The modified
	 */
	public float getModified() {
		return modified;
	}

	/**
	 * @param modified
	 * 		The modified
	 */
	public void setModified(float modified) {
		this.modified = modified;
	}

	/**
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * 		The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return The email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 * 		The email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return The emailNormalized
	 */
	public String getEmailNormalized() {
		return emailNormalized;
	}

	/**
	 * @param emailNormalized
	 * 		The email_normalized
	 */
	public void setEmailNormalized(String emailNormalized) {
		this.emailNormalized = emailNormalized;
	}

	/**
	 * @return The status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 * 		The status
	 */
	public void setStatus(String status) {
		this.status = status;
	}


}
