package com.celites.sharechannels.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Prasham on 4/25/2015.
 */
public class RecentPush {

	private boolean active;

	private float created;

	private float modified;

	private String type;

	private boolean dismissed;

	@SerializedName("sender_name")
	private String senderName;

	@SerializedName("channel_iden")
	private String channelIden;

	private String title;

	private String url;

	private String body;

	/**
	 * @return The active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 * 		The active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return The created
	 */
	public float getCreated() {
		return created;
	}

	/**
	 * @param created
	 * 		The created
	 */
	public void setCreated(float created) {
		this.created = created;
	}

	/**
	 * @return The modified
	 */
	public float getModified() {
		return modified;
	}

	/**
	 * @param modified
	 * 		The modified
	 */
	public void setModified(float modified) {
		this.modified = modified;
	}

	/**
	 * @return The type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 * 		The type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return The dismissed
	 */
	public boolean isDismissed() {
		return dismissed;
	}

	/**
	 * @param dismissed
	 * 		The dismissed
	 */
	public void setDismissed(boolean dismissed) {
		this.dismissed = dismissed;
	}

	/**
	 * @return The senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName
	 * 		The sender_name
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return The channelIden
	 */
	public String getChannelIden() {
		return channelIden;
	}

	/**
	 * @param channelIden
	 * 		The channel_iden
	 */
	public void setChannelIden(String channelIden) {
		this.channelIden = channelIden;
	}

	/**
	 * @return The title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 * 		The title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return The url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 * 		The url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return The body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 * 		The body
	 */
	public void setBody(String body) {
		this.body = body;
	}

}
