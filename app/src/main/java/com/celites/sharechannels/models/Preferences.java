package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class Preferences {

	private Onboarding onboarding;

	public Onboarding getOnboarding() {
		return onboarding;
	}

	public void setOnboarding(Onboarding onboarding) {
		this.onboarding = onboarding;
	}

	@Override
	public String toString() {
		return "ClassPojo [onboarding = " + onboarding + "]";
	}
}
